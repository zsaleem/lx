import { useEffect, useState } from 'react';
import axios from 'axios';

const URL = process.env.REACT_APP_URL;
const API = process.env.REACT_APP_API;
const ID = process.env.REACT_APP_ID;

/**
* Hooks that calls http request to get all the posts.
*
* @param page - current page number.
* @returns {Array, Function, boolean, boolean, string, number}
*/
export default function useFetcher(page = 0) {
	const [filterableData, setfilterableData] = useState<any>([]);
	const [spinner, setSpinner] = useState<boolean>(false);
	const [error, setError] = useState<string>();
	const [total, setTotal] = useState<number>();
	const [scrollerSpinner, setScrollerSpinner] = useState<boolean>(false);

	useEffect(() => {
		async function fetchData() {
			const response: any = await axios.get(`${URL}${API}?page=${page}`, {
				headers: {
					'app-id': ID as string,
				},
			});

			if (response.status === 200) {
				setfilterableData((prev) => [...prev, ...response.data.data]);
				setTotal(response.data.total);
				setSpinner(false);
				setScrollerSpinner(false);
				setError(undefined);
			} else {
				setError(response.statusText);
			}
		}
		if (page === 0 || page === 1) {
			setSpinner(true);
		} else {
			setScrollerSpinner(true);
		}
		fetchData();
	}, [page]);

	return {
		filterableData,
		setfilterableData,
		spinner,
		scrollerSpinner,
		error,
		total,
	};
}
