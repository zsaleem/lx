import Typography from '@mui/material/Typography';
import { makeStyles } from '@mui/styles';
import { styled } from '@mui/material/styles';

export const Container = styled(Typography)({
	
});

export const useStyles = makeStyles({
	fixed: {
		position: 'fixed',
		display: 'block',
		bottom: '0',
		left: '0',
		width: '100%',
		padding: '20px',
		textAlign: 'center',
		opacity: '.9',
		background: '#cccccc',
	},
	relative: {
		display: 'block',
		position: 'absolute',
		top: '50%',
		left: '50%',
	}
});
