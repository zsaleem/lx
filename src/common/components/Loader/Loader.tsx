import React from 'react';
import { Container, useStyles } from './styled';

interface LoaderProps {
	loadMore?: boolean;
	total?: number;
	loaded?: [];
}

/**
* Loader component that renders a preloader text.
*
* @param loadMore - To render loaderMore specific loader.
* @param total - To render if all the posts are rendered
* then shows a text that no more posts to be rendered.
* @param loaded - The loaded posts per page.
* 
* @returns JSX
*/
const Loader: React.FC<LoaderProps> = ({
	loadMore = false,
	total = 20,
	loaded = [],
}) => {
	const classes = useStyles();

	return (
		<Container
			className={loadMore ? classes.fixed : classes.relative }
		>
			{
				(loadMore && loaded.length >= total) ? (
					'No more posts to load'
				) : (
					'Loading...'
				)
			}
		</Container>
	);
};

export default Loader;
