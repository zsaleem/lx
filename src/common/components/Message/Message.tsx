import React from 'react';
import Typography from '@mui/material/Typography';

/**
* Message component that renders the message such as errors.
*
* @param message - Text that needs to be rendered.
* @returns JSX
*/
const Message: React.FC<{ message?: string }> = ({ message = '' }) => {
	return (
		<Typography>{message}</Typography>
	);
};

export default Message;
