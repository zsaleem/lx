import Typography from '@mui/material/Typography';
import { styled } from '@mui/material/styles';

export const PageTitle = styled(Typography)({
	fontSize: '30px',
	marginBottom: '20px',
});
