import React from 'react';
import { PageTitle } from './styled';

/**
* Title component that renders the title of the page.
*
* @param text - Text that needs to be rendered.
* @returns JSX
*/
const Title: React.FC<{ text?: string }> = ({ text = '' }) => {
	return (
		<PageTitle>{text}</PageTitle>
	);
};

export default Title;
