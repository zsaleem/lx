import React from 'react';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import Grid from '@mui/material/Grid';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useStyles } from './styled';

/**
* An interface with all the properties passed as props
* to SearchItem component.
*/
interface Items {
	items: {
		image: string;
		owner: {
			firstName: string;
			lastName: string;
			picture: string;
		};
		publishDate: string;
		text: string;
		map: (item) => { return;};
	}
}

/**
* Displays list of posts in Grid layout.
*
* @param Items - Props that has list of all objects about
* posts.
* @returns JSX
*/
const SearchItem: React.FC<Items> = ({ items }) => {
	const matches = useMediaQuery('(max-width:750px)');
	const classes = useStyles();
	
	return (
		<>
			{
				items?.map((item, index) => (
					<Grid item xs={matches ? 12 : 3} key={`${item.id}-${index}`}>
						<Card
							sx={{ maxWidth: 345 }}
							className={matches ? classes.responsive : classes.normal}>
							<CardHeader
								avatar={
									<Avatar
										sx={{ bgcolor: red[500] }}
										aria-label='recipe'
										src={item.owner.picture}
									>
										R
									</Avatar>
								}
								action={
									<IconButton aria-label='settings'>
										<MoreVertIcon />
									</IconButton>
								}
								title={`${item.owner.firstName} ${item.owner.lastName}`}
								subheader={new Date(item.publishDate).toLocaleDateString(
									undefined,
									{
										year: 'numeric',
										month: 'long',
										day: 'numeric',
									},
								)}
							/>
							<CardMedia
								component='img'
								height='194'
								image={item.image}
								alt='Paella dish'
							/>
							<CardContent>
								<Typography variant='body2' color='text.secondary'>
									{item.text}
								</Typography>
							</CardContent>
							<CardActions disableSpacing>
								<IconButton aria-label='add to favorites'>
									<FavoriteIcon />
								</IconButton>
								<IconButton aria-label='share'>
									<ShareIcon />
								</IconButton>
							</CardActions>
						</Card>
					</Grid>
				))
			}
		</>
	)
};

export default SearchItem;
