import React, { useEffect, useState, useCallback, useRef } from 'react';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Loader from '../../common/components/Loader/';
import Message from '../../common/components/Message/';
import Title from '../../common/components/Title/';
import SearchItem from './components/SearchItem/';
import useFetcher from '../../common/hooks/useFetcher/';
import useFilter from './hooks/useFilter/';
import { SearchField } from './styled';

/**
* Search page that returns JSX contents.
*/
const Search: React.FC = () => {
	/**
	* page state variable with setPage function to update
	* page.
	* 
	* @type number
	*/
	const [page, setPage] = useState<number>(0);
	/**
	* Destructure different properties from useFetch hook
	*/
	const {
		filterableData,
		spinner,
		scrollerSpinner,
		error,
		total,
	} = useFetcher(page);
	const [search] = useFilter();
	const loader = useRef<HTMLDivElement>(null);

	/**
	* Updates the page to fetch more data from API.
	*/
	const handleObserver = useCallback((entries) => {
		const target = entries[0];
		if (target.isIntersecting) {
			setPage((prev) => prev + 1);
		}
	}, []);

	/**
	* Gets called on on initial component mount and
	* whenever, handleObserver function is updated.
	* Calls handleObserver when the loader div element
	* is in the view.
	*/
	useEffect(() => {
		const option = {
			root: null,
			rootMargin: '20px',
			threshold: 0
		};

		const observer = new IntersectionObserver(handleObserver, option);
		if (loader.current) {
			observer.observe(loader.current);
		}
	}, [handleObserver]);

	return (
		<Container maxWidth='lg'>
			<Title text='Search'></Title>
			<SearchField
				id='outlined-search'
				label='Enter search query'
				type='search'
				onChange={search}
			/>
			<Grid container spacing={2}>
				{
					spinner ? (
						<Loader />
					) : (
						error ? (
							<Message message={error} />
						) : (
							<SearchItem items={filterableData} />
						)
					)
				}
				<div ref={loader} />
				{scrollerSpinner && <Loader loadMore={true} total={total} loaded={filterableData} />}
			</Grid>
		</Container>
	);
};

export default Search;
