import { useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import useFetcher from '../../../../common/hooks/useFetcher/';
import axios from 'axios';

const URL = process.env.REACT_APP_URL;
const API = process.env.REACT_APP_API;
const ID = process.env.REACT_APP_ID;

/**
* Filter hook that filters the results dynamically.
* 
* @returns [Function]
*/
export default function useFilter() {
	const { setfilterableData } = useFetcher(0);
	const history = useHistory<any>();
	const { query } = useParams<any>();

	useEffect(() => {
		/*
		async function filter() {
			if (query) {
				const response: any = await axios.get(`${URL}${API}?search=${query}`, {
					headers: {
						'app-id': ID as string,
					},
				});
				if (response.status === 200) {
					setfilterableData(response.data);
				}
			}
		}
		if (query) {
			filter();
		}
		*/
	}, [query]);

	const search = (event) => {
		history.push(event.target.value);
	};

	return [search];
}