import TextField from '@mui/material/TextField';
import { styled } from '@mui/material/styles';

export const SearchField = styled(TextField)({
	width: '100%',
	marginBottom: '40px',
});
