import * as React from 'react';
import { Switch, BrowserRouter, Route } from 'react-router-dom';

import Search from './pages/Search/';

export default function App() {
	return (
		<BrowserRouter>
			<Switch>
				<Route path='/' exact>
					<Search />
				</Route>
				<Route path='/:query'>
					<Search />
				</Route>
			</Switch>
		</BrowserRouter>
	);
}
