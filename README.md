## Requirements
Below is the stack I used in this project. Please follow the getting started section in order to make this project run.

## Stack
 - React
 - React Hooks
 - React Router DOM
 - JSX
 - CSS Grid
 - React UI Library/MUI
 - Axios
 - [Dummy API](https://dummyapi.io/docs)
 - CSS Media Queries
 - Mobile first responsive
 - ES6+
 - TypeScript
 - Git
 - Gitflow
 - Gitlab
 - Gitlab CI/CD
 - Heroku
 - TSDocs
 - NPM/Yarn
 - Sublime Text
 - Mac OS X
 - Google Chrome Incognito
 - eslint
 - nodejs(16.9.0)

## Getting started
In order to run this project in the browser successfully, please follow the steps below!

    1. Clone this repository.
    2. cd into `lx` directory.
    3. Run `yarn/npm install` command to download and install all dependencies.
    4. To run this project use `yarn/npm start:dev` command in command line.
    5. To build the project for production run `yarn/npm build` command. This will build the app for production and put all the files in `/build` folder.

### Description
Above steps, in getting started section, will install all the dependencies required for this project to run.

For this project I setup the whole development environment [create-react-app-with-typescript](https://github.com/mui-org/material-ui/tree/master/examples/create-react-app-with-typescript) boilerplate as it was described in the coding challenge.

In this task I used `React`. I made use of `React Hooks`, `React UI Library`, `TypeScript` and `React Router DOM` for routing. The list of pages goes into `/src/pages/` folder. Every page, `Search page` in this case, consist of its own folder with its page name. Components related to `Search page` resides inside `src/pages/Search` folder. `Hooks` specific to `Search page` is inside `src/pages/Search/hooks` folder. `src/common/components/` folder has all the components that are common to the entire application for reusability reasons. `src/common/hooks/` folder has all the hooks that are used throughout the project but in this case it is only one i.e. `useFetcher` hook. `/.env` file has environment variables for `base URL`, `API End Point`, `APP ID`. I implemented routing in `/app/App.js` file.

I also created a video from this project which shows the app in action. [Click here](https://youtu.be/AUaeGH_0q8E) to watch it in action.

You can also view this task in action over [here](https://lx-task.herokuapp.com/).

### Development Environments
As I mentioned I used [create-react-app-with-typescript](https://github.com/mui-org/material-ui/tree/master/examples/create-react-app-with-typescript) as it was suggested in the coding task explanation. I used `eslint` for code quality.

### Architecture
What I chose was `React hooks` based architecture, which is divided into pages and components. Since it is a small project so this was the best possible approach to go for other approached such as `Context API`, `Redux`, `useReducer` etc would be have been overkill for this project.

### Development process
The development process that I followed for this project is `Gitflow` work flow. That means, I have two git branches `master` and `develop`. So I created a `feature branch` from `develop` branch. I develop the whole feature in that branch. Once I am done with it. I `merge` that branch with `develop` branch and push it to remote repo with `develop` branch. When I need to release new feature, I create a new branch `release` with a version/tag number on it. Then I merge that `release` branch into master branch and push it to remote repo's `master` branch.

### DevOp
For DevOp, I used gitlab CI/CD and heroku to host the project. Since it is a small project for that reason I setup the CI/CD only for production environment. When I push changes to `master` branch, gitlab CI/CD triggers pipeline and builds and delploys the built version onto heroku.

### Code Quality
I wrote the entire project in components based architecture. I wrote all the code very readable and easy follow. I also used `TSDocs` to document the entire code. In addition, I used `eslint` to keep the quality of the code intact.

### Notes
Since one of the feature of this technical task was to implement filter system. For this project, I chose Dummy API for request list of posts to render them on the page. Since this API does not offer and search/filter end point for that reason I did implement a custom hook for it but it is not used because it does not have search/filter end point. Instead of going back and choosing another API which would require me to make several changes in my current implementation, I went on and used the Dummy API however, I added another feature to overcome the filter feature. Which is `Infinite Scroll`. I implemented the `Infinite Scroll` feature which you can have a look at in action.
